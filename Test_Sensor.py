#!/usr/bin/python

import spidev
import time
import RPi.GPIO as io 
from theThingsAPI import TheThingsAPI

#tt = TheThingsAPI("4bkiXvKRqn8ysH59Lyg87nZAjOKjp7a-_oZzqnaPC8k")

io.setmode(io.BCM) 


#configuration
# The spi bus to which the mcp3008 is connected.
spi_bus = 0
# The device number on the bus.
mcp3008_spi_device = 0
# The pin on the mcp3008 to which the light diode is connected.
light_channel = 0

# This is in seconds.
update_delay = 10



# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):
    adc = spi.xfer2([1, (8 + channel) << 4, 0])
    data = ((adc[1] & 3) << 8) + adc[2]
    return data


# Function to convert data to voltage level,
# rounded to specified number of decimal places.
def ConvertVolts(data, places):
    volts = (data * 3.3) / float(1023)
    volts = round(volts, places)
    return volts


# Open SPI bus
spi = spidev.SpiDev()
spi.open(spi_bus, mcp3008_spi_device)




while True:


    light_channel_level = ReadChannel(light_channel)
    light_channel_volts = ConvertVolts(light_channel_level, 4)


#try:
    print('light sensor level')
    print(light_channel_level)
 #   tt.addVar('light_sensor_level', light_channel_level)
    
    print('light sensor volt')
    print(light_channel_volts)
 #   tt.addVar('light_sensor_volt', light_channel_volts)
    
    
 #   tt.write();
    
#except:
    print('Could not communicate with server')
    

    # Wait before repeating loop
    time.sleep(update_delay)