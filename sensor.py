#!/usr/bin/python

import bme280
import bme280_i2c
import spidev
import time
import RPi.GPIO as io 
from theThingsAPI import TheThingsAPI

tt = TheThingsAPI("4bkiXvKRqn8ysH59Lyg87nZAjOKjp7a-_oZzqnaPC8k")

io.setmode(io.BCM) 


#configuration
# The spi bus to which the mcp3008 is connected.
spi_bus = 0
# The device number on the bus.
mcp3008_spi_device = 0
# The pin on the mcp3008 to which the light diode is connected.
light_channel = 0
# The pin on the mcp3008 to which the soil humidty sensor is connected.
soil_channel = 1
# Typical bus on Raspberry Pi is 1.
i2c_bus = 1
# Retrieve this info using i2cdetect -y 1
bme280_address = 0x77


# This is in seconds.
update_delay = 10



# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):
    adc = spi.xfer2([1, (8 + channel) << 4, 0])
    data = ((adc[1] & 3) << 8) + adc[2]
    return data


# Function to convert data to voltage level,
# rounded to specified number of decimal places.
def ConvertVolts(data, places):
    volts = (data * 3.3) / float(1023)
    volts = round(volts, places)
    return volts


# Open SPI bus
spi = spidev.SpiDev()
spi.open(spi_bus, mcp3008_spi_device)



# Set up communication to BME280.
bme280_i2c.set_default_bus(i2c_bus)
#bme280_i2c.set_default_i2c_address(int(bme280_address, 0))
bme280_i2c.set_default_i2c_address(int('0x77',0))
bme280.setup()


# Control power for motor 
pir_pin = 24 
power_pin = 23
 
io.setup(pir_pin, io.IN) 
io.setup(power_pin, io.OUT)
io.output(power_pin, False)



while True:

    soil_channel_level = ReadChannel(soil_channel)
    soil_channel_volts = ConvertVolts(soil_channel_level, 4)

    light_channel_level = ReadChannel(light_channel)
    light_channel_volts = ConvertVolts(light_channel_level, 4)

    all_bme280_data = bme280.read_all()


#try:
    print('light sensor level')
    print(light_channel_level)
    tt.addVar('light_sensor_level', light_channel_level)
    
    print('light sensor volt')
    print(light_channel_volts)
    tt.addVar('light_sensor_volt', light_channel_volts)
    
    print('soil sensor level')
    print(soil_channel_level)
    tt.addVar('soil_sensor_level', soil_channel_level)
    
    print('soil sensor volt')
    print(soil_channel_volts)
    tt.addVar('soil_sensor_volt', soil_channel_volts)
    
    print('air pressure')
    print(all_bme280_data.pressure)
    tt.addVar('air_pressure', all_bme280_data.pressure)
    
    print('air humidity')
    print(all_bme280_data.humidity)
    tt.addVar('air_humidity',  all_bme280_data.humidity)
    
    print('air temperature')
    print(all_bme280_data.temperature)
    tt.addVar('air_temperature',  all_bme280_data.temperature)
    
    
    if soil_channel_level>=704:
        print("POWER ON")
        io.output(power_pin, True)
        time.sleep(5);
        tt.addVar('power',  1)
        tt.write();
        print("POWER OFF")
        io.output(power_pin, False)
        time.sleep(5)
    else: 
        tt.addVar('power',  0)
    
    tt.write();
    
#except:
    print('Could not communicate with server')
    

    # Wait before repeating loop
    time.sleep(update_delay)